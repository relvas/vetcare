# VetCare

APPLICATION CONTEXT

This application's main objective is to not only help veterinary doctors and nurses in their everyday jobs but also monitor hospitalized animals, manage medical consultations and emergencies. 
One of the best features about this app is the quick and easy access to each animal characteristics without the need of asking the owners all of them every single time. It is as simple as searching with the name or chip number and you get access to all the animal data (allergies, previous hospitalizations.. ).
The app makes the monitoring of the hospitalized animals so much easier since it allows the user to add alarms to medications with the quantities and all the information related with it. It is also possible to add notes related with each animal that is being monitored at the time.
Another problem solved by us here is the coordination of the team work that is guaranteed by allowing the users to define a calendar with cirurgical interventions, normal day-to-day appointments or even single nursing acts, since all of this data is shared and can be seen by all of the co-workers in the company.

APPLICATION PURPOSE

Log in of each team member
Search for an animal
Search for cirurgical interventions, normal day-to-day appointments or even single nursing acts
Alarms for vet related stuff


SENSOR & MOBILE FUNCTIONALITIES

QR -  Each animal has a QR code for a faster check up
PHOTO - Each animal has a profile picture
MAP - Animal habitation 
ALARM - Animal Medication/Nursing act
CALENDAR - Add some procedure related to vet doctor/nurse