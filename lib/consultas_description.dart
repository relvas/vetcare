import 'package:flutter/material.dart';
import 'package:login/consultas.dart';
import 'package:login/tab_cont_doctor.dart';

class Consultas_Description extends StatelessWidget{
  static String tag = 'consulta-description-page';
  int index;

  Consultas_Description({Key key, @required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return new DefaultTabController(
      length: 3,
      child:
        Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.teal,
            title: Text('Dr. Laura'),
            leading: new IconButton(
                icon: new Icon(Icons.close),
                onPressed: () {
                  Navigator.of(context).pop();
                }
            ),
          ),
          body: new Scrollbar(child: new SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child:
            Column(
                children: [
                  Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.fromLTRB(10.0, 70.0, 0.0, 60.0),
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Container(
                                width: 100.0,
                                height: 100.0,
                                decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                        fit: BoxFit.fill,
                                        image: new NetworkImage(Consultas.animals[this.index].pic)
                                    )
                                )),
                          ],
                        ),
                        Padding(
                          padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                        ),
                        Text (Consultas.animals[this.index].name, textAlign: TextAlign.left, style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),)
                      ],
                    ),
                  Row(children: [
                    Padding(
                          padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Nome: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].name, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                        padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Dono: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].dono, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Espécie: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].especie, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Raça: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].raca, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Sexo: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].sexo, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Idade: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].idade.toStringAsFixed(0) + " anos", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Data: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].data_consulta + 'h', textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Vacinado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].vacinado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Desparasitado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].desparasitado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30),
                    ),
                    new Text("Peso: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Consultas.animals[this.index].peso.toStringAsFixed(2) + " kg", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  botao_diagonostico(index)
                ]
            ),)
        ))
    );
  }
}

class botao_diagonostico extends StatelessWidget{
  int index;

  botao_diagonostico(this.index);

  @override
  Widget build(BuildContext context){
    return new Container(
      padding: new EdgeInsets.only(right: 185, top: 185, bottom: 10),

      child: RaisedButton.icon(
        color: Colors.teal,
        textColor: Colors.white,
        icon: const Icon(Icons.remove),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(48),
        ),
        label: const Text('Terminar consulta'),//inciar consulta
        onPressed: () {
          Consultas.animalsFirstDay.removeAt(index);
          Navigator.of(context).pop();
        },
      ),

    );
  }
}

