import 'package:flutter/material.dart';
import 'package:login/Animal.dart';
import 'package:login/tab_cont_doctor.dart';
import 'package:login/internados.dart';
import 'package:login/internado.dart';


class add_internados extends StatefulWidget {
  static String tag = 'add-internados';
  int index;
  add_internados({Key key, @required this.index}) : super(key: key);
  _MenuInt createState() => new _MenuInt();
}
class _MenuInt extends State<add_internados>{
  int index;
  String dropdownValueEsp;
  String dropdownValueSex;
  String dropdownValueVac;
  String dropdownValueDes;
  String dropdownValueEnf;
  String dropdownValueBox;
  String dropdownValueEst;
  final textControllerNomeAnimal = new TextEditingController();
  final textControllerNomeDono = new TextEditingController();
  final textControllerRaca = new TextEditingController();
  final textControllerIdade = new TextEditingController();
  final textControllerPeso = new TextEditingController();
  final textControllerSexo = new TextEditingController();
  final textControllerDesp = new TextEditingController();
  final textControllerVac = new TextEditingController();
  final textControllerEsp = new TextEditingController();
  final textControllerEnf = new TextEditingController();
  final textControllerFebre = new TextEditingController();
  final textControllerMed = new TextEditingController();
  final textControllerDiag = new TextEditingController();
  final textControllerEst = new TextEditingController();
  final textControllernBox = new TextEditingController();

  void _addToListInt(){
    Internado novoInternado = new Internado(textControllerNomeAnimal.text, textControllerNomeDono.text, textControllerEsp.text, textControllerRaca.text, textControllerSexo.text, int.parse(textControllerIdade.text), int.parse(textControllernBox.text), textControllerEnf.text, DateTime.now().toString().substring(0,16) , textControllerVac.text, textControllerDesp.text, double.parse(textControllerPeso.text), double.parse(textControllerFebre.text), textControllerMed.text, textControllerEst.text, textControllerDiag.text, 'https://bit.ly/2LhPrcA');
    Internados.animals.add(novoInternado);
  }


  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 3,
      child:
      Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: Text('Dr. Laura'),
        ),
        body: new Scrollbar(child: new SingleChildScrollView(
        scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget> [
              new Padding(padding: EdgeInsets.only(top: 30.0)),
              new Text('Adicionar Animal Internado', textAlign: TextAlign.left, style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold)),
              new Padding(padding: EdgeInsets.only(top: 30)),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerNomeAnimal,
                    decoration: new InputDecoration(
                      labelText: "Nome do animal",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(

                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerNomeDono,
                    decoration: new InputDecoration(
                      labelText: "Nome do Proprietário",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Espécie:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValueEsp,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValueEsp = newValue;
                          // textControllerEsp.text = newValue;
                        });
                      },
                      hint: Text("Animal"),
                      items: <String>['Cão', 'Gato', 'Outro']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerRaca,
                    decoration: new InputDecoration(
                      labelText: "Raça",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Sexo:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValueSex,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValueSex = newValue;
                          textControllerSexo.text = newValue;
                        });
                      },
                      hint: Text("Sexo"),
                      items: <String>['M', 'F']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerIdade,
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(
                      labelText: "Idade",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerPeso,
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(
                      labelText: "Peso",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Vacinado:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValueVac,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValueVac = newValue;
                          textControllerVac.text = newValue;
                        });
                      },
                      hint: Text("Vacinado"),
                      items: <String>['Sim', 'Não']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Desparasitado:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValueDes,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValueDes = newValue;
                          textControllerDesp.text = newValue;
                        });
                      },
                      hint: Text("Desparasitação"),
                      items: <String>['Interno', 'Externo', 'Interno/Externo', 'Nenhum']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Enfermeiro:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValueEnf,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValueEnf = newValue;
                          textControllerEnf.text = newValue;
                        });
                      },
                      hint: Text("Enfermeiro"),
                      items: <String>['Raquel', 'Diogo']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Número da caixa:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValueBox,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValueBox = newValue;
                          textControllernBox.text = newValue;
                        });
                      },

                      hint: Text("Nº"),
                      items: <String>['1','3','4','5','6']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerFebre,
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(
                      labelText: "Temperatura",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerMed,
                    decoration: new InputDecoration(
                      labelText: "Medicação",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Estado:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValueEst,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValueEst = newValue;
                          textControllerEst.text = newValue;
                        });
                      },
                      hint: Text("Estado"),
                      items: <String>['Recuperado', 'Em recuperação']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),


              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerDiag,
                    decoration: new InputDecoration(
                      labelText: "Diagnóstico",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                padding: new EdgeInsets.only(right: 195, top: 90, bottom: 10),
                child: FloatingActionButton.extended(

                  backgroundColor: Colors.teal,
                  foregroundColor: Colors.white,
                  icon: const Icon(Icons.add),
                  label: const Text('Animal Internado'),
                  onPressed: () {
                    _addToListInt();
                    Navigator.of(context).pop();
                  },
                ),

              ),
            ],
          ),
        ),
      ),)
    );

  }
}
