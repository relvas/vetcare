import 'package:flutter/material.dart'; // Dá import das packages em vez dos ficheiros
import 'package:login/tab_cont_doctor.dart';
import 'package:login/login_page.dart';

import 'package:login/consultas.dart';
import 'package:login/operacoes.dart';
import 'package:login/internados.dart';
import 'package:login/tarefas.dart';
import 'package:login/search_animal.dart';

import 'package:login/consultas_description.dart';
import 'package:login/internados_description.dart';
import 'package:login/Operacao_Description.dart';
import 'package:login/Tarefas_Description.dart';

import 'package:login/Animal.dart';
import 'package:login/Operation.dart';
import 'package:login/internado.dart';
import 'package:login/Tarefa.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  void initVars(){

    /*Consultas*/
    Animal poopy = new Animal('Poopy', 'Susana Dias', 'Cão', 'Pequeno A', 'F', 8, '02/04/2019 12:00', 'Nenhuma', 'Interior/Exterior', 6.3, 'https://bit.ly/2PKzgD5');
    Animal noah = new Animal('Noah', 'Rafael Oliveira', 'Gato', 'Siames', 'M', 5, '02/04/2019 15:30', 'Nenhuma', 'Exterior', 2.5, 'https://bit.ly/2LhPrcA');
    Animal myia = new Animal('Myia', 'Rui Oliveira', 'Gato', 'Siames', 'F', 7, '14/06/2019 15:30', 'Nenhuma', 'Exterior', 2.75, 'https://bit.ly/2LhPrcA');
    Consultas.animalsFirstDay = new List <Animal>();
    Consultas.animalsFirstDay = [poopy, noah];
    Consultas.animalsOtherDay = [myia];

    /*Operacoes*/
    Operation marley = new Operation('Marley', 'Castelo Branco', 'Cão', 'Caniche', 'M', 8, '02/04/2019 16:00', 'Sim', 'Interior', 'Anestesia', 5.3, 'Osso partido, pata direita frente', 'https://bit.ly/2vH5aHH');
    Operation sushi = new Operation('Sushi', 'Maria João', 'Gato', 'Europeu Comum', 'M', 5, '02/04/2019 17:30', 'Nenhuma', 'Exterior', 'Anestesia', 2.5, 'Esterilização', 'https://bit.ly/2Y20Dvy');
    Operation toby = new Operation('Toby', 'João André', 'Cão', 'Caniche', 'M', 5, '02/04/2019 11:30', 'Nenhuma', 'Exterior', 'Anestesia', 2.5, 'Esterilização', 'https://bit.ly/2Y20Dvy');
    Operacoes.animalsFirstDay = new List <Operation>();
    Operacoes.animalsOtherDay = new List <Operation>();
    Operacoes.animalsFirstDay = [sushi, marley];
    Operacoes.animalsOtherDay = [toby];

    /*Internados*/
    Internado ace = new Internado('Ace', 'António Dias', 'Cão', 'Serra da Estrela', 'M', 8, 7, 'Diogo', '23/03/2019 14:00', 'Sim', 'Interior', 8.1, 38.5, 'Soro', 'Recuperado', 'Constipação e vómitos', 'https://bit.ly/2vAExnr');
    Internado amy = new Internado('Amy', 'João Tavares', 'Gato', 'Europeu Comum', 'M', 5, 8, 'Raquel','27/03/2019 11:30', 'Nenhuma', 'Nenhuma', 2.3, 40.3, 'Soro', 'Em Recuperação', 'Febre e vómitos', 'https://bit.ly/2Y7i51L');
    Internado chungus = new Internado('Chungus', 'Filipe Tavares', 'Gato', 'Europeu Comum', 'M', 5, 2, 'Diogo','27/03/2019 15:30', 'Nenhuma', 'Nenhuma', 2.3, 40.3, 'Soro', 'Em Recuperação', 'Vómitos', 'https://bit.ly/2Y7i51L');
    Internados.animals = new List <Internado>();
    Internados.animals = [amy, ace, chungus];

    /*Tarefas*/
    Tarefa ace_tarefa = new Tarefa('Ace', 'António Dias', 'Cão', 'Serra da Estrela', 'M', 8, 'Sim', 'Interior', 'Soro', 8.1, 7, '23/03/2019 15:30',  'Constipação e vómitos', 'Recuperado', 'Necessário troca de soro de 8 em 8 horas', 'Diogo', 'https://bit.ly/2vAExnr');
    Tarefa amy_tarefa = new Tarefa('Amy', 'João Tavares', 'Gato', 'Europeu Comum', 'M', 5, 'Nenhuma', 'Nenhuma', 'Soro', 2.3, 8, '23/03/2019 15:30',  'Febre e vómitos', 'Em Recuperação', 'Necessário troca de soro de 8 em 8 horas', 'Raquel', 'https://bit.ly/2Y7i51L');
    Tarefa chungus_tarefa = new Tarefa('Chungus', 'Filipe Tavares', 'Gato', 'Europeu Comum', 'M', 5, 'Nenhuma', 'Nenhuma', 'Soro', 2.3, 2, '23/03/2019 15:30', 'Vómitos', 'Em Recuperação', 'Necessário troca de soro de 8 em 8 horas', 'Diogo', 'https://bit.ly/2Y7i51L');
    Tarefas.tarefas = new List <Tarefa>();
    Tarefas.tarefas = [ace_tarefa , chungus_tarefa, amy_tarefa];

    /*Animais no sistema*/

    Search_animal.animalsInTheSystem = new List<Animal>();

    Animal marleyAn = new Animal(marley.name,marley.dono,marley.especie,marley.raca,marley.sexo,marley.idade,marley.data_operacao,marley.vacinado,marley.desparasitado,marley.peso,marley.pic);
    Animal sushiAn = new Animal(sushi.name,sushi.dono,sushi.especie,sushi.raca,sushi.sexo,sushi.idade,sushi.data_operacao,sushi.vacinado,sushi.desparasitado,sushi.peso,sushi.pic);
    Animal aceAn = new Animal(ace.name,ace.dono,ace.especie,ace.raca,ace.sexo,ace.idade,ace.data_internamento,ace.vacinado,ace.desparasitado,ace.peso,ace.pic);
    Animal amyAn = new Animal(amy.name,amy.dono,amy.especie,amy.raca,amy.sexo,amy.idade,amy.data_internamento,amy.vacinado,amy.desparasitado,amy.peso,amy.pic);
    Animal chungusAn = new Animal(chungus.name,chungus.dono,chungus.especie,chungus.raca,chungus.sexo,chungus.idade,chungus.data_internamento,chungus.vacinado,chungus.desparasitado,chungus.peso,chungus.pic);

    Search_animal.animalsInTheSystem..addAll(Consultas.animalsFirstDay);
    Search_animal.animalsInTheSystem..addAll(Consultas.animalsOtherDay);
    Search_animal.animalsInTheSystem.add(marleyAn);
    Search_animal.animalsInTheSystem.add(sushiAn);
    Search_animal.animalsInTheSystem.add(aceAn);
    Search_animal.animalsInTheSystem.add(amyAn);
    Search_animal.animalsInTheSystem.add(chungusAn);

  }

  @override
  Widget build(BuildContext context) {
    initVars();

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: LoginPage(),
      routes: <String, WidgetBuilder>{
        LoginPage.tag : (BuildContext context) => new LoginPage(),
        Consultas.tag : (BuildContext context) => new Consultas(),
        Operacoes.tag : (BuildContext context) => new Operacoes(),
        Internados.tag : (BuildContext context) => new Internados(),
        Consultas_Description.tag : (BuildContext context) => new Consultas_Description(),
        Operacao_Description.tag : (BuildContext context) => new Operacao_Description(),
        internados_description.tag : (BuildContext context) => new internados_description(),
        Tarefas_Description.tag : (BuildContext context) => new Tarefas_Description(),
        TabControllerPageDoctor.tag : (BuildContext context) => new TabControllerPageDoctor(),
    }
    );
  }
}