

class Internado{
  String name;
  String dono;
  String especie;
  String raca;
  String sexo;
  int idade;
  int nrBox;
  String enfermeiro_responsavel;
  String data_internamento;
  String vacinado;
  String desparasitado;
  double peso;
  double temperatura;
  String medicacao;
  String estado;
  String diagonostico;
  String pic;

  Internado(String name, String dono, String especie, String raca, String sexo, int idade, int nrBox, String enfermeiro_responsavel, String data_internamento, String vacinado, String desparasitado, double peso, double temperatura, String medicacao,  String estado,  String diagonostico, String pic){
    this.name = name;
    this.dono = dono;
    this.especie = especie;
    this.raca = raca;
    this.sexo = sexo;
    this.idade = idade;
    this.nrBox = nrBox;
    this.enfermeiro_responsavel = enfermeiro_responsavel;
    this.data_internamento = data_internamento;
    this.vacinado = vacinado;
    this.desparasitado = desparasitado;
    this.peso = peso;
    this.temperatura = temperatura;
    this.medicacao = medicacao;
    this.estado = estado;
    this.diagonostico = diagonostico;
    this.pic = pic;
  }
}