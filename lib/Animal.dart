

class Animal {

  String name;
  String dono;
  String especie;
  String raca;
  String sexo;
  int idade;
  String data_consulta;
  String vacinado;
  String desparasitado;
  double peso;
  String pic;

  Animal( String name, String dono, String especie, String raca, String sexo, int idade, String data_consulta, String vacinado, String desparasitado, double peso, String pic ){
    this.name = name;
    this.dono = dono;
    this.especie = especie;
    this.raca = raca;
    this.sexo = sexo;
    this.idade = idade;
    this.data_consulta = data_consulta;
    this.vacinado = vacinado;
    this.desparasitado = desparasitado;
    this.peso = peso;
    this.pic = pic;
  }
}