import 'package:flutter/material.dart';
import 'package:login/tab_cont_doctor.dart';
import 'package:login/Animal.dart';
import 'package:login/consultas.dart';
import 'package:login/search_animal.dart';
import 'package:login/operacoes.dart';
import 'package:login/Operation.dart';
import 'package:table_calendar/table_calendar.dart';

class Add_Time extends StatefulWidget {
  static String action;
  static String OperationDesc;
  static String medicacao;

  @override
  Add_Time_State createState() => Add_Time_State();
}

class Add_Time_State extends State<Add_Time> with TickerProviderStateMixin {
  DateTime _selectedDay;
  AnimationController _controller;
  String title;

  @override
  void initState() {
    super.initState();
    _selectedDay = DateTime.now();

    _controller = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );

    _controller.forward();

    if(Add_Time.action.split('-')[0] == 'OperationSearch'){
      title = "Agendar Operação";
    }else{
      title = "Agendar Consulta";
    }
  }

  Future<Null> _onDaySelected(DateTime day, List events) async {
    setState(() {
      _selectedDay = day;
    });

    TimeOfDay time = await showTimePicker(
        context: context, initialTime: TimeOfDay.now());

    DateTime date = new DateTime(day.year, day.month, day.day, time.hour, time.minute);
    Animal animal;
    Operation operation;
    if (Add_Time.action == 'NovoAnimal') {
      Consultas.animalsOtherDay.last.data_consulta =
          date.toString().replaceAll(':00.000', '');
      _showDialog(context, "Consulta");

    }else if (Add_Time.action == 'NovaOperacao') {
      Operacoes.animalsOtherDay.last.data_operacao =
          date.toString().replaceAll(':00.000', '');
      _showDialog(context, "Operação");

    }else if(Add_Time.action.split('-')[0] == 'ConsultasSearch'){
      animal = Search_animal.animalsInTheSystem.firstWhere((a) => a.name == Add_Time.action.split('-')[1]);
      animal.data_consulta = date.toString().replaceAll(':00.000', '');
      Consultas.animalsOtherDay.add(animal);
      _showDialog(context, "Consulta");

    }else if(Add_Time.action.split('-')[0] == 'OperationSearch'){ //ALL FUCKED
      animal = Search_animal.animalsInTheSystem.firstWhere((a) => a.name == Add_Time.action.split('-')[1]);
      animal.data_consulta = date.toString().replaceAll(':00.000', '');

      operation = new Operation(animal.name, animal.dono, animal.especie, animal.raca, animal.sexo, animal.idade, animal.data_consulta, animal.vacinado, animal.desparasitado, Add_Time.medicacao, animal.peso, Add_Time.OperationDesc , animal.pic);
      Operacoes.animalsOtherDay.add(operation);
      _showDialog(context, "Operação");
    }
  }

  _showDialog(BuildContext context, String type){
    showDialog(
        context: context,
        builder: (_) {
          return new AlertDialog(
            title: new Text(type + " adicionada com sucesso"),
            actions: <Widget>[
              new FlatButton(
                textColor: Colors.teal,
                child: new Text("Ok"),
                onPressed: () {
                  Navigator.of(context).pop();
                  Navigator.push(context, MaterialPageRoute( builder: (context) => TabControllerPageDoctor()));
                },
              ),
            ],
          );
        }
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        backgroundColor: Colors.teal,
        title: new Text(title),
      ),
      body: Column(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          // Switch out 2 lines below to play with TableCalendar's settings
          //-----------------------
          _buildTableCalendar(),
          //_buildTableCalendarWithBuilders(),
          const SizedBox(height: 8.0),
        ],
      ),
    );
  }

  // Simple TableCalendar configuration (using Styles)
  Widget _buildTableCalendar() {
    return TableCalendar(
      locale: 'en_US',
      initialCalendarFormat: CalendarFormat.month,
      formatAnimation: FormatAnimation.slide,
      startingDayOfWeek: StartingDayOfWeek.monday,
      availableGestures: AvailableGestures.all,   // para Meter Gesture: horizontal ou vertical
      availableCalendarFormats: const {
        CalendarFormat.month: 'Month',
        CalendarFormat.twoWeeks: '2 weeks',       // para mostrar apenas 2 semanas
        CalendarFormat.week: 'Week',
      },
      calendarStyle: CalendarStyle(
        selectedColor: Colors.teal[400],
        todayColor: Colors.teal[100],
        markersColor: Colors.teal[700],
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle: TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Colors.teal[400],
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      onDaySelected: _onDaySelected,
    );
  }

}