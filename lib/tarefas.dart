import 'package:flutter/material.dart';
import 'package:login/Tarefa.dart';
import 'package:login/Tarefas_Description.dart';
import 'package:login/tab_cont_enfermeiro.dart';

class Tarefas extends StatelessWidget {
  static String tag = 'tarefas-page';

  static List <Tarefa> tarefas;

  List <String> names = [];
  List <int> nBox = [];
  List <String> enfermeiroResp = [];

  void initList() {

    for (var tarefa in tarefas) {
      if (!(names.contains(tarefa.name)) && TabControllerPageEnfermeiro.nome_empregado == ("Enfermeiro " + tarefa.enfermeiroResp)){
        names.add(tarefa.name);
        nBox.add(tarefa.nBox);
        enfermeiroResp.add(tarefa.enfermeiroResp);
      }
    }
  }

  @override
  Widget build(BuildContext context) {

    initList();
    return new Column(
        children: [
            Padding(
              padding : const EdgeInsets.only(top: 16.0),
            ),
            Row(
              children: <Widget>[
                Expanded(
                child:
                  new Text("Animal", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0))
                ),
                Expanded(
                child:
                  new Text("NºBox", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0))
                ),
                Expanded(
                child:
                  new Text("Enfermeiro", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0))
                ),
                Padding(
                  padding : const EdgeInsets.only(top: 16.0),
                ),
            ]),
            Padding(
              padding : const EdgeInsets.only(top: 8.0),
            ),
            Expanded(
                child: new ListView.builder(
                  itemBuilder: (_, int index) => EachList(index, this.names[index], this.nBox[index], this.enfermeiroResp[index]),
                  itemCount: this.names.length,
                )),
        ]);
  }
}

class EachList extends StatelessWidget{
  final int index;
  final String name;
  final int nBox;
  final String enfermeiroResp;

  EachList(this.index, this.name, this.nBox, this.enfermeiroResp);
  @override
  Widget build(BuildContext context) {

    return new GestureDetector(
        onTap: (){
          print("Container pressed");
          Navigator.push(context, MaterialPageRoute( builder: (context) => Tarefas_Description(index : this.index)));
        },
        child : new Card(

            margin: const EdgeInsets.fromLTRB(10,8,10,8),
            child:
            new Container(
                padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                child : new Row(
                  children: [
                    Expanded(
                        child:
                        new Text(name, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                    Expanded(
                        child:
                        new Text(nBox.toString(), textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                    Expanded(
                        child:
                        new Text(enfermeiroResp, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                  ],
                )
            )
        )
    );
  }
}

