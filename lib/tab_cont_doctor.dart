import 'package:flutter/material.dart';
import 'package:login/consultas.dart';
import 'package:login/operacoes.dart';
import 'package:login/internados.dart';
import 'package:login/login_page.dart';
import 'package:login/calendar.dart';

class TabControllerPageDoctor extends StatefulWidget {
  static String tag = 'tab-doc-page';
  static String nome_empregado;

  @override
  TabControllerState createState() => new TabControllerState();
}

class TabControllerState extends State<TabControllerPageDoctor> {

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            indicatorColor : Colors.white,
            tabs: [
              Tab(text: 'Consultas'),
              Tab(text: 'Operações'),
              Tab(text: 'Animais internados'),
            ],
          ),
          backgroundColor: Colors.teal,
          title: Text(TabControllerPageDoctor.nome_empregado),
          leading:
          new RotationTransition(
            turns: new AlwaysStoppedAnimation(180 / 360),
            child: new IconButton(
              icon: new Icon(Icons.exit_to_app),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute( builder: (context) => LoginPage()));
              }
          ),),
          actions: <Widget>[
            IconButton(
              icon: new Icon(Icons.calendar_today),
              onPressed: () {
                Navigator.push(context, MaterialPageRoute( builder: (context) => Calendar()));
              }
          ),]
        ),
        body: TabBarView(
          children: [
            Consultas(),
            Operacoes(),
            Internados()
          ],
        ),
      ),
    );
  }

}