import 'package:flutter/material.dart';
import 'package:login/tarefas.dart';
import 'package:login/add_tarefas.dart';

class Tarefas_Description extends StatelessWidget{
  static String tag = 'tarefas-description-page';
  int index;

  Tarefas_Description({Key key, @required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    double c_width = MediaQuery.of(context).size.width*0.8;
    return new DefaultTabController(
        length: 3,
        child:
        Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.teal,
              title: Text('Enfermeiro Diogo'),
              leading: new IconButton(
                  icon: new Icon(Icons.close),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }
              ),
            ),
            body: new Scrollbar(child: new SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child: Column(

                children: [
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(10.0, 70.0, 0.0, 60.0),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          new Container(
                              width: 100.0,
                              height: 100.0,
                              decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                      fit: BoxFit.fill,
                                      image: new NetworkImage(Tarefas.tarefas[this.index].pic)
                                  )
                              )),
                        ],
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                      ),
                      Text (Tarefas.tarefas[this.index].name, textAlign: TextAlign.left, style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),)
                    ],
                  ),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Nome: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].name, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Dono: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].dono, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Espécie: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].especie, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Raça: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].raca, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Sexo: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].sexo, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Idade: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].idade.toStringAsFixed(0) + " anos", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Vacinado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].vacinado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Desparasitado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].desparasitado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Medicação: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].medicacao, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Agendada para: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].data_tarefa, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Número da caixa: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].nBox.toString(), textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Estado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].estado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Enfermeiro responsável: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].enfermeiroResp, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Diagonóstico: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  ],),
                  Container(
                    width: c_width,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        new Text(Tarefas.tarefas[this.index].diagonostico, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),),
                      ],
                    ),),
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                      ),
                      new Text("Descrição da tarefa: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    ],),
                  Container(
                    width: c_width,
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                        new Text(Tarefas.tarefas[this.index].descricao_tarefa, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),),
                    ],
                  ),),
                  Row( children :[
                    botao_alta(index),
                    botao_alarme(index)
                  ]
                  ),
                ]
            ),)
        ))
    );
  }
}

class botao_alta extends StatelessWidget{
  final int index;

  botao_alta(this.index);

  @override
  Widget build(BuildContext context){
    return new Container(
      padding: new EdgeInsets.only(left: 10, top: 65, bottom: 10),
      child: new RaisedButton.icon(
        textColor: Colors.white,
        icon: Icon(Icons.remove),
        label: const Text('Terminar tarefa'),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(48),
        ),

        color: Colors.teal,
        onPressed: () {
          Tarefas.tarefas.removeAt(index);
          Navigator.of(context).pop();
        },
      ),
    );
  }
}

class botao_alarme extends StatelessWidget{
  final int index;

  botao_alarme(this.index);

  @override
  Widget build(BuildContext context){
    return new Container(
      padding: new EdgeInsets.only(left: 60, top: 65, bottom: 10),

      child: RaisedButton.icon(
        color: Colors.teal,
        textColor: Colors.white,
        icon: const Icon(Icons.add),
        label: const Text('Adicionar Tarefa'),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(48),
        ),
        onPressed: () {
          add_task.name = Tarefas.tarefas[this.index].name;
          add_task.nome_empregado = 'Enfermeiro Diogo';
          Navigator.push(context, MaterialPageRoute( builder: (context) => add_task()));
        },
      ),

    );
  }
}



