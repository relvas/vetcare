import 'package:flutter/material.dart';
import 'package:login/tab_cont_doctor.dart';
import 'package:login/tab_cont_enfermeiro.dart';
import 'package:login/internados_description.dart';
import 'package:login/consultas.dart';
import 'package:login/operacoes.dart';

class LoginPage extends StatefulWidget {
  static String tag = 'login-page';

  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  int error= 0;

  final textController = new TextEditingController();

  bool _submit(){
    if(textController.text == 'laura'){
      internados_description.nome_empregado = 'Dr. Laura';
      TabControllerPageDoctor.nome_empregado = 'Dr. Laura';
      print(textController.text);
      Consultas.whatList=1;
      Consultas.date="Hoje";
      Operacoes.date="Hoje";
      Navigator.push(context, MaterialPageRoute( builder: (context) => TabControllerPageDoctor()));
      return true;
    }else if(textController.text == 'diogo'){
      internados_description.nome_empregado = 'Enfermeiro Diogo';
      TabControllerPageEnfermeiro.nome_empregado = 'Enfermeiro Diogo';
      Navigator.push(context, MaterialPageRoute( builder: (context) => TabControllerPageEnfermeiro()));
      return true;
    }else{
      print("false");
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    final logo = Hero(
      tag: 'hero',
      child: CircleAvatar(
        backgroundColor: Colors.transparent,
        radius: 150.0,
        child: Image.asset('assets/logo.png'),
      ),
    );

    final username = TextFormField(
      cursorColor: Colors.black,
      controller: textController,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Username',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.teal, width: 1.5), borderRadius: BorderRadius.circular(32.0)),
        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0), borderSide: BorderSide(color: Colors.teal, width: 1.0)),
      ),
    );

    final password = TextFormField(
      cursorColor: Colors.black,
      autofocus: false,
      obscureText: true,
      decoration: InputDecoration(
        hintText: 'Password',
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
        focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.teal, width: 1.5), borderRadius: BorderRadius.circular(32.0)),
        enabledBorder: OutlineInputBorder(borderRadius: BorderRadius.circular(30.0), borderSide: BorderSide(color: Colors.teal, width: 1.0)),
      ),
    );

    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () => _submit(),
        padding: EdgeInsets.fromLTRB(12,15,12,15),
        color: Colors.teal,
        child: Text('Log In', style: TextStyle(color: Colors.white)),
      ),
    );

    final forgotLabel = FlatButton(
      child: Text(
        'Forgot password?',
        style: TextStyle(color: Colors.black54),
      ),
      onPressed: () {},
    );

    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            logo,
            SizedBox(height: 48.0),
            username,
            SizedBox(height: 8.0),
            password,
            SizedBox(height: 24.0),
            loginButton,
            forgotLabel,
          ],
        ),
      ),
    );
  }
}