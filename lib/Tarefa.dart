

class Tarefa{

  String name;
  String dono;
  String especie;
  String raca;
  String sexo;
  int idade;
  String vacinado;
  String desparasitado;
  String medicacao;
  double peso;
  int nBox;
  String data_tarefa;
  String diagonostico;
  String estado;
  String descricao_tarefa;
  String enfermeiroResp;
  String pic;

  Tarefa( String name, String dono, String especie, String raca, String sexo, int idade, String vacinado, String desparasitado, String medicacao, double peso, int nBox, String data_tarefa, String diagonostico, String estado, String descricao_tarefa, String enfermeiroResp, String pic){
    this.name = name;
    this.dono = dono;
    this.especie = especie;
    this.raca = raca;
    this.sexo = sexo;
    this.idade = idade;
    this.vacinado = vacinado;
    this.desparasitado = desparasitado;
    this.medicacao = medicacao;
    this.peso = peso;
    this.data_tarefa = data_tarefa;
    this.nBox = nBox;
    this.diagonostico = diagonostico;
    this.estado = estado;
    this.descricao_tarefa = descricao_tarefa;
    this.enfermeiroResp = enfermeiroResp;
    this.pic = pic;
  }
}