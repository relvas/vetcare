import 'package:flutter/material.dart';
import 'package:login/consultas.dart';
import 'package:login/Animal.dart';
import 'package:login/add_time.dart';


class add_consultas extends StatefulWidget {
  static String tag = 'add_con-tag';
  int index;
  add_consultas({Key key, @required this.index}) : super(key: key);
  _Menu createState() => new _Menu();
}
class _Menu extends State<add_consultas>{
  int index;
  String dropdownValue;
  String dropdownValue1;
  String dropdownValue2;
  String dropdownValue3;
  final textControllerNomeAnimal = new TextEditingController();
  final textControllerNomeDono = new TextEditingController();
  final textControllerRaca = new TextEditingController();
  final textControllerIdade = new TextEditingController();
  final textControllerPeso = new TextEditingController();
  final textControllerSexo = new TextEditingController();
  final textControllerDesp = new TextEditingController();
  final textControllerVac = new TextEditingController();
  final textControllerEsp = new TextEditingController();

  void _addToList(){
    Animal novoAnimal = new Animal(textControllerNomeAnimal.text, textControllerNomeDono.text, textControllerEsp.text, textControllerRaca.text, textControllerSexo.text, int.parse(textControllerIdade.text), '02/04/2019 15:30h', textControllerVac.text, textControllerDesp.text, double.parse(textControllerPeso.text), 'https://bit.ly/2LhPrcA');
    Consultas.animals.add(novoAnimal);
  }


  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 3,
      child:
      Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.teal,
          title: Text('Dr. Laura'),
        ),
        body: new Scrollbar(child: new SingleChildScrollView(
        scrollDirection: Axis.vertical,
          child: Column(
            children: <Widget> [
              new Padding(padding: EdgeInsets.only(top: 30.0)),
              new Text('Marcar Consulta', textAlign: TextAlign.left, style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold)),
              new Padding(padding: EdgeInsets.only(top: 30)),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerNomeAnimal,
                    decoration: new InputDecoration(
                      labelText: "Nome do animal",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerNomeDono,
                    decoration: new InputDecoration(
                      labelText: "Nome do Proprietário",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Espécie:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValue,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue = newValue;
                          textControllerEsp.text = newValue;
                        });
                      },
                      hint: Text("Animal"),
                      items: <String>['Cão', 'Gato', 'Outro']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerRaca,
                    decoration: new InputDecoration(
                      labelText: "Raça",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Sexo:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValue1,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue1 = newValue;
                          textControllerSexo.text = newValue;
                        });
                      },
                      hint: Text("Sexo"),
                      items: <String>['M', 'F']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerIdade,
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(
                      labelText: "Idade",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),
              new Container(
                child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
                  child: TextFormField(
                    cursorColor: Colors.black,
                    controller: textControllerPeso,
                    keyboardType: TextInputType.number,
                    decoration: new InputDecoration(
                      labelText: "Peso",
                      labelStyle: TextStyle(fontSize: 18, color: Colors.grey,),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.5),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(30.0),
                        borderSide: BorderSide(color: Colors.teal, width: 1.0),
                      ),
                    ),
                  ),
                ),
              ),

              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Vacinado:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValue2,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue2 = newValue;
                          textControllerVac.text = newValue;
                        });
                      },

                      hint: Text("Vacinado"),
                      items: <String>['Sim', 'Não']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
              new Row(
                  children: <Widget>[
                    Padding(padding : const EdgeInsets.only(left: 20.0),),
                    Text('Desparasitado:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                    Padding(padding : const EdgeInsets.only(left: 16.0),),
                    DropdownButton<String>(
                      value: dropdownValue3,
                      onChanged: (String newValue) {
                        setState(() {
                          dropdownValue3 = newValue;
                          textControllerDesp.text = newValue;
                        });
                      },
                      hint: Text("Desparasitação"),
                      items: <String>['Interno', 'Externo', 'Interno/Externo']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                        );
                      }).toList(),
                    ),
                  ]),
               new Container(
                  padding: new EdgeInsets.only(right: 195, top: 90, bottom: 10),
                  child: FloatingActionButton.extended(
                    backgroundColor: Colors.teal,
                    foregroundColor: Colors.white,
                    icon: const Icon(Icons.date_range),
                    label: const Text('Agendar Consulta'),
                    onPressed: () {
                      _addToList();
                      Add_Time.action='NovoAnimal';
                      Navigator.push(context, MaterialPageRoute( builder: (context) => Add_Time()));
                    },
                  ),

                ),
            ],
          ),
        ),
      ))
    );

  }
}
