import 'package:flutter/material.dart';
import 'package:login/operacoes.dart';

class Operacao_Description extends StatelessWidget{
  static String tag = 'operacao-description-page';
  int index;

  Operacao_Description({Key key, @required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return new DefaultTabController(
        length: 3,
        child:
        Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.teal,
            title: Text('Dr. Laura'),
            leading: new IconButton(
                icon: new Icon(Icons.close),
                onPressed: () {
                  Navigator.of(context).pop();
                }
            ),
          ),
          body: new Scrollbar(child: new SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child:
              Column(
              children: [
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(10.0, 70.0, 0.0, 60.0),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage(Operacoes.animals[this.index].pic)
                                )
                            )),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                    ),
                    Text (Operacoes.animals[this.index].name, textAlign: TextAlign.left, style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),)
                  ],
                ),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Nome: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].name, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Dono: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].dono, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Espécie: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].especie, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Raça: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].raca, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Sexo: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].sexo, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Idade: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].idade.toStringAsFixed(0) + " anos", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Data: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].data_operacao + "h", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Vacinado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].vacinado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Desparasitado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].desparasitado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Medicação: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].medicacao, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Peso: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Operacoes.animals[this.index].peso.toStringAsFixed(2) + " kg", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(
                  children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),

                  new Text("Descrição da operação: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                ],),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 40.0, top: 30, right: 10.0),
                    ),
                    new Flexible(child: new Scrollbar(child: new SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                    child:
                      new Text(Operacoes.animals[this.index].descricao_operacao, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),),
                    )))
                  ],
                ),
                botao_operacao(index)
              ]
          ),)
        ))
    );
  }
}

class botao_operacao extends StatelessWidget{
  final int index;

  botao_operacao(this.index);

  @override
  Widget build(BuildContext context){
    return new Container(
      padding: new EdgeInsets.only(right: 195, top: 90, bottom: 10),

      child: RaisedButton.icon(
        color: Colors.teal,
        textColor: Colors.white,
        icon: const Icon(Icons.remove),
        label: const Text('Terminar operação'),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(48),
        ),
        onPressed: () {
          Operacoes.animals.removeAt(index);
          Navigator.of(context).pop();
        },
      ),

    );
  }
}



