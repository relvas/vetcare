import 'package:flutter/material.dart';
import 'package:login/internado.dart';
import 'package:login/internados_description.dart';
import 'package:login/add_internados.dart';

class Internados extends StatelessWidget{
  static String tag = 'internados-page';

  static List <Internado> animals;

  List <Align> estado = [];
  List <String> names = [];
  List <String> donos = [];
  List <String> enfermeiro = [];

  void initList() {
    for (var animal in animals) {
      if (!(names.contains(animal.name))){
        if(animal.estado == "Recuperado") {
          estado.add(Align(
              alignment: Alignment.centerLeft,
              child:
              Padding(
                padding : const EdgeInsets.only(left: 30.0),
                  child: Icon(Icons.check, color: Colors.green)
              ),
          ));
        }else{
          estado.add(Align(
              alignment: Alignment.centerLeft,
              child:
              Padding(
                  padding : const EdgeInsets.only(left: 30.0),
                  child: Icon(Icons.clear, color: Colors.red)
              ),
          ));
        }
        names.add(animal.name);
        donos.add(animal.dono);
        enfermeiro.add(animal.enfermeiro_responsavel);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    initList();
    return new Column(
        children: [
          Padding(
            padding : const EdgeInsets.only(top: 16.0),
          ),
          Row(
              children: <Widget>[
                Expanded(
                    child:
                    new Text("Estado", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),)
                ),
                Expanded(
                    child:
                    new Text("Animal", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0))
                ),
                Expanded(
                    child:
                    new Text("Dono", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0))
                ),
                Expanded(
                    child:
                    new Text("Enfermeiro", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0))
                ),
                Padding(
                  padding : const EdgeInsets.only(right: 16.0),
                ),
              ]),
          Padding(
            padding : const EdgeInsets.only(top: 8.0),
          ),

          Expanded(
              child: new ListView.builder(
                itemBuilder: (_, int index) => EachList(index, this.estado[index], this.names[index], this.donos[index], this.enfermeiro[index]),
                itemCount: this.names.length,
              )),
          plusbutton(),
        ]);
  }
}

class EachList extends StatelessWidget{
  int index;
  final Align estado;
  final String name;
  final String dono;
  final String enfermeiro;
  EachList(this.index, this.estado, this.name, this.dono, this.enfermeiro);
  @override
  Widget build(BuildContext context) {

    return new GestureDetector(
        onTap: (){
          print("Container pressed");
          Navigator.push(context, MaterialPageRoute( builder: (context) => internados_description(index : this.index)));
        },
        child : new Card(

            margin: const EdgeInsets.fromLTRB(10,8,10,8),
            child:
            new Container(
                padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                child : new Row(
                  children: <Widget>[
                    Expanded(
                        child:
                        estado
                    ),
                    Expanded(
                        child:
                        new Text(name, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                    Expanded(
                        child:
                        new Text(dono.split(' ')[0], textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                    Expanded(
                        child:
                        new Text(enfermeiro, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                  ],
                )
            )
        )
    );
  }
}

class plusbutton extends StatelessWidget{
  @override
  Widget build(BuildContext context){
    if(internados_description.nome_empregado == 'Enfermeiro Diogo') return new Container();
    return  new Container(
      padding: new EdgeInsets.only(left: 300, bottom: 20),
      child: new FloatingActionButton(
        child: Icon(Icons.add,),
        backgroundColor: Colors.teal,
        foregroundColor: Colors.white,
        onPressed: () {

          Navigator.push(context, MaterialPageRoute( builder: (context) => add_internados()));
        },
      ),
    );
  }
}
