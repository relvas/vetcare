import 'package:flutter/material.dart';
import 'package:login/tarefas.dart';
import 'package:login/internados.dart';
import 'package:login/login_page.dart';
import 'package:login/add_tarefas.dart';
import 'dart:async';

class TabControllerPageEnfermeiro extends StatefulWidget {
  static String tag = 'tab-enfermeiro-page';
  static String nome_empregado;
  static int notificationCounter;
  static bool notification;

  @override
  _TabControllerState createState() => new _TabControllerState();
}

class _TabControllerState extends State<TabControllerPageEnfermeiro> {
  int notificationCount = 0;
  Timer _timer;

  void startVerification() {
    const oneSec = const Duration(seconds: 1);
    _timer = new Timer.periodic(
        oneSec,
            (Timer timer) => setState(() {
        if(add_task.scheduledNotificationDateTime!=null && TabControllerPageEnfermeiro.notification!=null){
          if (TabControllerPageEnfermeiro.notification && add_task.scheduledNotificationDateTime.isBefore(DateTime.now()) ) {
            notificationCount += 1;
            TabControllerPageEnfermeiro.notification = false;
            add_task.scheduledNotificationDateTime = null;
            print(TabControllerPageEnfermeiro.notification);
            print(add_task.scheduledNotificationDateTime);
          }
        }
        }));
  }

  @override
  initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    startVerification();

    return new DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          bottom: TabBar(
            indicatorColor : Colors.white,
            tabs: [
              Tab(text: 'Tarefas'),
              Tab(text: 'Internados'),
            ],
          ),
          backgroundColor: Colors.teal,
          title: Text(TabControllerPageEnfermeiro.nome_empregado),
          leading:
          new RotationTransition(
            turns: new AlwaysStoppedAnimation(180 / 360),
            child: new IconButton(
                icon: new Icon(Icons.exit_to_app),
                onPressed: () {
                  Navigator.push(context, MaterialPageRoute( builder: (context) => LoginPage()));
                }
            ),),
            actions: <Widget>[
              Stack(
                children: [
                  IconButton(
                      padding: const EdgeInsets.only(top: 8),
                      icon: new Icon(Icons.notifications, size: 28.0),
                      onPressed: () {
                        if(notificationCount >= 1){
                          notificationCount -= 1;
                        }
                        Navigator.push(context, MaterialPageRoute( builder: (context) => TabControllerPageEnfermeiro()));
                        showDialog(
                          context: context,
                          builder: (_) {
                            return new AlertDialog(
                              title: new Text("Nova Tarefa, " +add_task.nameTarefa),
                              content: new Text(add_task.description),
                              actions: <Widget>[
                                new FlatButton(
                                  textColor: Colors.teal,
                                  child: new Text("Ok"),
                                  onPressed: () {
                                    Navigator.of(context).pop();
                                  },
                                ),
                              ],
                            );
                          },
                        );
                      }
                  ),
                  Container(
                    padding: const EdgeInsets.only(left: 10),
                    alignment: Alignment.topRight,
                    margin: EdgeInsets.only(top: 15),
                    child: Container(
                      width: 15,
                      height: 15,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color(0xffc32c37),
                          border: Border.all(color: Colors.white, width: 1)),
                      child: Padding(
                        padding: const EdgeInsets.all(0.0),
                        child: Center(
                          child: Text(
                            notificationCount.toString(),
                              style: TextStyle(fontSize: 10),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),]
        ),
        body: TabBarView(
          children: [
            Tarefas(),
            Internados(),
          ],
        ),
      ),
    );
  }

 }