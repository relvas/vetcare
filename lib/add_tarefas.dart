import 'package:flutter/material.dart';
import 'dart:async';
import 'package:login/tarefas.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:login/Tarefa.dart';
import 'package:login/tab_cont_enfermeiro.dart';


class add_task extends StatefulWidget {
  static String tag = 'add-tasks';
  static String name;
  static DateTime scheduledNotificationDateTime;
  static String nome_empregado;
  static String description;
  static String nameTarefa;

  _MenuTask createState() => new _MenuTask();
}

class _MenuTask extends State<add_task>{

  FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin;
  DateTime scheduledNotificationDateTime;

  final textControllerDesc = new TextEditingController();
  final textControllerDate = new TextEditingController();
  String dropdownValue;
  int index;
  Tarefa a;
  Duration d;

  void _addToList(){

    d = new Duration(seconds:  2 );
    //d = new Duration(seconds:  int.parse(textControllerDate.text.split(' ')[0]) );

    scheduledNotificationDateTime = new DateTime.now().add(d);

    TabControllerPageEnfermeiro.notification = true;
    add_task.scheduledNotificationDateTime = scheduledNotificationDateTime;
    print("DATE: " +scheduledNotificationDateTime.toString());

    Tarefa n = new Tarefa(a.name, a.dono, a.especie, a.raca, a.sexo, a.idade, a.vacinado, a.desparasitado, a.medicacao, a.peso, a.nBox, scheduledNotificationDateTime.toString().substring(0,16) ,a.diagonostico, a.estado, textControllerDesc.text, a.enfermeiroResp, a.pic);
    Tarefas.tarefas[index] = n;
  }

  @override
  initState() {
    super.initState();
    var initializationSettingsAndroid = new AndroidInitializationSettings('logo');
    var initializationSettingsIOS = new IOSInitializationSettings();
    var initializationSettings = new InitializationSettings(initializationSettingsAndroid, initializationSettingsIOS);
    flutterLocalNotificationsPlugin = new FlutterLocalNotificationsPlugin();
    flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: onSelectNotification);
  }

  @override
  Widget build(BuildContext context) {

    a = Tarefas.tarefas.firstWhere((a) => a.name == add_task.name);
    index = Tarefas.tarefas.indexOf(a);

    print(add_task.name);
    print(Tarefas.tarefas.last.data_tarefa);
    print(Tarefas.tarefas.last.name);
    return new DefaultTabController(
        length: 3,
        child:
        Scaffold(
            appBar: AppBar(
              backgroundColor: Colors.teal,
              title: Text(add_task.nome_empregado),
              leading: new IconButton(
                  icon: new Icon(Icons.close),
                  onPressed: () {
                    Navigator.of(context).pop();
                  }
              ),
            ),
            body: new Scrollbar(child: new SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child:
            Column(
                children: [
                  Row(
                    children: [
                      Padding(
                        padding: EdgeInsets.fromLTRB(10.0, 70.0, 0.0, 60.0),
                      ),
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Container(
                              width: 100.0,
                              height: 100.0,
                              decoration: new BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage(Tarefas.tarefas[index].pic)
                                  )
                              )), ],
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                      ),
                      Text (Tarefas.tarefas[index].name, textAlign: TextAlign.left, style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),)
                    ],
                  ),
                  Row(children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Enfermeiro Responsável: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                    new Text(Tarefas.tarefas[this.index].enfermeiroResp, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                  ],),

                  new Container(
                    child: Padding(padding: EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 10.0),
                      child: TextFormField(
                        cursorColor: Colors.black,
                        controller: textControllerDesc,
                        decoration: new InputDecoration(
                          labelText: "Descrição da tarefa",
                          labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            borderSide: BorderSide(color: Colors.teal, width: 1.5),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30.0),
                            borderSide: BorderSide(color: Colors.teal, width: 1.0),
                          ),
                        ),
                      ),
                    ),
                  ),
                  new Row(
                      children: <Widget>[
                        Padding(padding : const EdgeInsets.only(left: 20.0, top: 80),),
                        Text('Agendar:', style: TextStyle(fontSize: 20.0, color: Colors.grey,)),
                        Padding(padding : const EdgeInsets.only(left: 16.0, top: 80),),
                        DropdownButton<String>(
                          value: dropdownValue,
                          onChanged: (String newValue) {
                            setState(() {
                              dropdownValue = newValue;
                              textControllerDate.text = newValue;
                            });
                          },
                          hint: Text("Daqui a"),
                          items: <String>['30 segundos', '1 hora', '2 horas', '3 horas', '4 horas']
                              .map<DropdownMenuItem<String>>((String value) {
                            return DropdownMenuItem<String>(
                              value: value,
                              child: Text(value, style: TextStyle(fontSize: 15.0, color: Colors.grey[850]),),
                            );
                          }).toList(),
                        ),
                      ]),
                  new Container(
                    padding: new EdgeInsets.only(right: 195, top: 50, bottom: 10),

                    child: FloatingActionButton.extended(
                      backgroundColor: Colors.teal,
                      foregroundColor: Colors.white,
                      icon: const Icon(Icons.assignment),
                      label: const Text('Adicionar Tarefa'),
                      onPressed: () {
                        _addToList();
                        _showNotification();

                        Navigator.of(context).pop();
                        Navigator.of(context).pop();
                      },
                    ),

                  ),
                ]
            ),)
        ))
    );

  }

  Future <void> onSelectNotification(String payload) async {
    showDialog(
      context: context,
      builder: (_) {
        return new AlertDialog(
          title: Text("Nova Tarefa"),
          content: Text(textControllerDesc.text),
        );
      },
    );
  }

  Future <void>  _showNotification() async {

    var androidPlatformChannelSpecifics = new AndroidNotificationDetails(
      'CHANNEL_ID',
      'CHANNEL_NAME',
      "CHANNEL_DESCRIPTION",
      importance: Importance.Max,
      priority: Priority.High,
      playSound: true,
      timeoutAfter: 5000,
      styleInformation: DefaultStyleInformation(true, true),
    );
    var iOSPlatformChannelSpecifics = new IOSNotificationDetails(presentSound: false);
    var platformChannelSpecifics = new NotificationDetails(androidPlatformChannelSpecifics, iOSPlatformChannelSpecifics);

    add_task.nameTarefa = a.name;
    add_task.description = textControllerDesc.text;
    await flutterLocalNotificationsPlugin.schedule(
        0,
        'Nova Tarefa, ' + a.name + '\n',
        textControllerDesc.text,
        scheduledNotificationDateTime,
        platformChannelSpecifics,
        payload: textControllerDesc.text
    );
  }

}
