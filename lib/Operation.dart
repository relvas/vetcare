

class Operation implements Comparable<Operation>{

  String name;
  String dono;
  String especie;
  String raca;
  String sexo;
  int idade;
  String data_operacao;
  String vacinado;
  String desparasitado;
  String medicacao;
  double peso;
  String descricao_operacao;
  String pic;

  Operation(String name, String dono, String especie, String raca, String sexo, int idade, String data_operacao, String vacinado, String desparasitado, String medicacao, double peso, String descricao_operacao, String pic){
    this.name = name;
    this.dono = dono;
    this.especie = especie;
    this.raca = raca;
    this.sexo = sexo;
    this.idade = idade;
    this.data_operacao = data_operacao;
    this.vacinado = vacinado;
    this.desparasitado = desparasitado;
    this.medicacao = medicacao;
    this.peso = peso;
    this.descricao_operacao = descricao_operacao;
    this.pic = pic;
  }


  int compareTo(Operation other) {
    int order = data_operacao.compareTo(other.data_operacao);
    return order;
  }

}