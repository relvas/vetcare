import 'package:flutter/material.dart';
import 'package:login/Animal.dart';
import 'package:login/add_time.dart';
import 'package:dio/dio.dart';

class Search_animal extends StatefulWidget {

  static List <Animal> animalsInTheSystem;
  static String action;

  @override
  Search_animal_State createState() => Search_animal_State();
}

class Search_animal_State extends State<Search_animal>  {


  final TextEditingController _filter = new TextEditingController();
  final dio = new Dio();
  String _searchText = "";
  List names = new List();
  List filteredNames = new List();
  Icon _searchIcon = new Icon(Icons.search);
  Widget _appBarTitle;

  Search_animal_State() {
    _filter.addListener(() {
      if (_filter.text.isEmpty) {
        setState(() {
          _searchText = "";
          filteredNames = names;
        });
      } else {
        setState(() {
          _searchText = _filter.text;
        });
      }
    });
  }

  @override
  void initState() {
    this._appBarTitle = new TextField(
      style: TextStyle(color: Colors.white),
      cursorColor: Colors.white,
      controller: _filter,
      decoration: new InputDecoration(
          focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.teal, width: 0)),
          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.teal, width: 0)),
          prefixIcon: new Icon(Icons.search, color: Colors.white),
          hintText: "Pesquisa...",
          hintStyle: new TextStyle(color: Colors.white)
      ),
    );
    this._getNames();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _buildBar(context),
      body:
      Container(
        child: _buildList(),
      ),
      resizeToAvoidBottomPadding: false,
    );
  }

  Widget _buildBar(BuildContext context) {
    return new AppBar(
      backgroundColor: Colors.teal,
      centerTitle: true,
      title: _appBarTitle,
      leading:
        IconButton(
            icon: new Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.of(context).pop();
            }
        ),
    );
  }


  void _getNames() async {
    setState(() {
      names = Search_animal.animalsInTheSystem;
      filteredNames = names;
    });
  }


  Widget _buildList() {
    if (!(_searchText.isEmpty)) {
      List tempList = new List();
      for (int i = 0; i < filteredNames.length; i++) {
        if (filteredNames[i].name.toLowerCase().contains(_searchText.toLowerCase())) {
          tempList.add(filteredNames[i]);
        }
      }
      filteredNames = tempList;
    }

    return new Column(
        children: [
          Padding(
            padding : const EdgeInsets.only(top: 16.0),
          ),
          Row(
              children: <Widget>[
                Expanded(
                    child:
                    new Text("Animal", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),)
                ),
                Expanded(
                    child:
                    new Text("Dono", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),)
                ),

              ]),
          Padding(
          padding : const EdgeInsets.only(top: 10.0),
          ),
          Expanded(
              child: new ListView.builder(
                itemBuilder: (_, int index) => EachList(index, this.filteredNames[index].name, this.filteredNames[index].dono),
                itemCount: names == null ? 0 : filteredNames.length,
              )),
    ]);
  }
}

class EachList extends StatelessWidget{

  final int index;
  final String name;
  final String dono;

  final textControllerDesc = new TextEditingController();
  final textControllerMed = new TextEditingController();
  EachList(this.index, this.name, this.dono);

  _inputs(context){
    return new Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          new Container(
            child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
              child: TextFormField(
                cursorColor: Colors.black,
                controller: textControllerDesc,
                decoration: new InputDecoration(
                  labelText: "Descrição da operação",
                  labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide(color: Colors.teal, width: 1.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide(color: Colors.teal, width: 1.0),
                  ),
                ),
              ),
            ),
          ),
          new Container(
            child: Padding(padding: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 10.0),
              child: TextFormField(
                cursorColor: Colors.black,
                controller: textControllerMed,
                decoration: new InputDecoration(
                  labelText: "Medicação",
                  labelStyle: TextStyle(fontSize: 18, color: Colors.grey),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide(color: Colors.teal, width: 1.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(30.0),
                    borderSide: BorderSide(color: Colors.teal, width: 1.0),
                  ),
                ),
              ),
            ),
          ),

        ]);
  }

  _showDialog(BuildContext context){
    showDialog(context: context,
      child: new AlertDialog(
        title: new Text("Informação sobre Operação"),
        content: _inputs(context),
        actions: <Widget>[
          new FlatButton(
            textColor: Colors.teal,
            child: new Text("Adicionar"),
            onPressed: () {
              Add_Time.OperationDesc = textControllerDesc.text;
              Add_Time.medicacao = textControllerMed.text;
              Navigator.of(context).pop();
              Navigator.push(
                  context, MaterialPageRoute(builder: (context) => Add_Time()));
            },
          ),
        ],
      ),);
  }

  @override
  Widget build(BuildContext context) {

    return new GestureDetector(
        onTap: (){
          Add_Time.action=Search_animal.action +'-'+ name;
          if(Search_animal.action == "OperationSearch"){
            _showDialog(context);
          }else {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Add_Time()));
          }
        },
        child : new Card(
            child:
            new Container(
                padding: EdgeInsets.only(top: 17.0, bottom: 17.0),
                child : new Row(
                  children: <Widget>[
                    Expanded(
                        child:
                        new Text(name, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                    Expanded(
                        child:
                        new Text(dono, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                  ],
                )
            )
        )
    );
  }
}
