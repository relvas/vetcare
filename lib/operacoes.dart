import 'package:flutter/material.dart';
import 'package:login/Operation.dart';
import 'package:login/Operacao_Description.dart';
import 'package:login/add_operacoes.dart';
import 'package:login/search_animal.dart';
import 'package:login/consultas.dart';
import 'package:login/tab_cont_doctor.dart';

class Operacoes extends StatelessWidget{
  static String tag = 'operacoes-page';

  static int whatList=1;
  static List <Operation> animals;
  static List <Operation> animalsFirstDay;
  static List <Operation> animalsOtherDay;
  static String date;

  List <String> names = [];
  List <String> donos = [];
  List <String> hora = [];

  void initList() {
    if(whatList==2) animals=animalsOtherDay;
    else animals=animalsFirstDay;
    if(animals != null){
      animals.sort();
      for (var animal in animals) {
        if (!(names.contains(animal.name))){
          names.add(animal.name);
          donos.add(animal.dono);
          hora.add(animal.data_operacao.split(" ")[1]);
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    initList();

    Widget _backToToday(BuildContext context){
      if(Operacoes.whatList == 1) return new Container();
      return new Container(
          padding: new EdgeInsets.only(right: 20),
          child:
          RaisedButton.icon(
              color: Colors.white,
              textColor: Colors.black,
              icon: const Icon(Icons.today),
              label: const Text('Ir para hoje'),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(38),
              ),
              onPressed: () {
                Consultas.whatList = 1;
                Consultas.date="Hoje";
                Operacoes.whatList = 1;
                Operacoes.date="Hoje";
                Navigator.push(context, MaterialPageRoute( builder: (context) => TabControllerPageDoctor()));
              }
          )
      );
    }

    return new Column(
        children: [
          Padding(
            padding : const EdgeInsets.only(top: 16.0),
          ),
          Row(
            children: <Widget>[
              Padding(
                padding : const EdgeInsets.all(10.0),
              ),
              Expanded(
                  child: Row(children: <Widget> [
                    Icon(Icons.date_range),
                    Padding(
                      padding : const EdgeInsets.all(3.0),
                    ),
                    new Text(date, textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0)),
                  ])
              ),
              Expanded(
                  child:
                  _backToToday(context)
              )
            ],
          ),
          Padding(
            padding : const EdgeInsets.only(top: 10.0),
          ),
          Row(
              children: <Widget>[
                Expanded(
                    child:
                    new Text("Animal", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),)
                ),
                Expanded(
                    child:
                    new Text("Dono", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),)
                ),
                Expanded(
                    child:
                    new Text("Hora", textAlign: TextAlign.center, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.0),)
                ),
                Padding(
                  padding : const EdgeInsets.only(top: 36.0),
                ),
              ]),
          Expanded(
              child: new ListView.builder(
                itemBuilder: (_, int index) => EachList(index, this.names[index], this.donos[index], this.hora[index]),
                itemCount: this.names.length,
              )),
          plusbutton()
        ]);
  }
}

class EachList extends StatelessWidget{
  final int index;
  final String name;
  final String dono;
  final String hora;
  EachList(this.index, this.name, this.dono, this.hora);
  @override
  Widget build(BuildContext context) {

    return new GestureDetector(
        onTap: (){
          print("Container pressed");
          Navigator.push(context, MaterialPageRoute( builder: (context) => Operacao_Description(index : this.index)));
        },
        child : new Card(

            margin: const EdgeInsets.fromLTRB(10,8,10,8),
            child:
            new Container(

                padding: EdgeInsets.only(top: 20.0, bottom: 20.0),
                child : new Row(
                  children: <Widget>[
                    Expanded(
                        child:
                        new Text(name, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                    Expanded(
                        child:
                        new Text(dono.split(' ')[0], textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                    Expanded(
                        child:
                        new Text(hora, textAlign: TextAlign.center, style: TextStyle(fontSize: 20.0),)
                    ),
                  ],
                )
            )
        )
    );
  }
}

class plusbutton extends StatelessWidget{


  @override
  Widget build(BuildContext context){
    return  new Container(
      padding: new EdgeInsets.only(left: 300, bottom: 20),
      child: new FloatingActionButton(
        child: Icon(Icons.add,),
        backgroundColor: Colors.teal,
        foregroundColor: Colors.white,
        onPressed: () {
          showDialog(context: context,
            child: new AlertDialog(
              title: new Text("Adicionar Operação"),
              content: new Text("O Animal já está registado?"),
              actions: <Widget>[
                // usually buttons at the bottom of the dialog
                new FlatButton(
                  textColor: Colors.teal,
                  child: new Text("Sim"),
                  onPressed: () {
                    Search_animal.action = 'OperationSearch';
                    Navigator.of(context).pop();
                    Navigator.push(context, MaterialPageRoute( builder: (context) => Search_animal()));
                  },
                ),
                new FlatButton(
                  textColor: Colors.teal,
                  child: new Text("Não"),
                  onPressed: () {
                    Navigator.of(context).pop();
                    Navigator.push(context, MaterialPageRoute( builder: (context) => add_operacoes()));
                  },
                ),
              ],
            ),);
        },
      ),
    );
  }
}