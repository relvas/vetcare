import 'package:flutter/material.dart';
import 'package:login/internados.dart';
import 'package:login/add_tarefas.dart';

class internados_description extends StatelessWidget{
  static String tag = 'internados_description-page';
  int index;
  static String nome_empregado;

  internados_description({Key key, @required this.index}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return new DefaultTabController(
        length: 3,
        child:
        Scaffold(
          appBar: AppBar(
            backgroundColor: Colors.teal,
            title: Text(internados_description.nome_empregado),
            leading: new IconButton(
                icon: new Icon(Icons.close),
                onPressed: () {
                  Navigator.of(context).pop();
                }
            ),
          ),
          body: new Scrollbar(child: new SingleChildScrollView(
              scrollDirection: Axis.vertical,
              child:
          Column(
              children: [
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.fromLTRB(10.0, 70.0, 0.0, 60.0),
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                            width: 100.0,
                            height: 100.0,
                            decoration: new BoxDecoration(
                                shape: BoxShape.circle,
                                image: new DecorationImage(
                                    fit: BoxFit.fill,
                                    image: new NetworkImage(Internados.animals[this.index].pic)
                                )
                            )),
                      ],
                    ),
                    Padding(
                      padding: EdgeInsets.fromLTRB(15.0, 0.0, 0.0, 0.0),
                    ),
                    Text (Internados.animals[this.index].name, textAlign: TextAlign.left, style: TextStyle(fontSize: 30.0, fontWeight: FontWeight.bold),)
                  ],
                ),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Nome: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].name, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Dono: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].dono, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Espécie: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].especie, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Raça: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].raca, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Sexo: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].sexo, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Idade: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].idade.toStringAsFixed(0) + " anos", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Data de internamento: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].data_internamento, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Enfermeiro Responsável: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].enfermeiro_responsavel, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Número da caixa: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].nrBox.toStringAsFixed(0), textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Vacinado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].vacinado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Desparasitado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].desparasitado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Medicação: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].medicacao, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Peso: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].peso.toStringAsFixed(2) + " kg", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(children: [
                  Padding(
                    padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                  ),
                  new Text("Estado: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  new Text(Internados.animals[this.index].estado, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),)
                ],),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 10.0, top: 30, right: 10.0),
                    ),
                    new Text("Diagonóstico: ", textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),),
                  ],),
                Row(
                  children: [
                    Padding(
                      padding: EdgeInsets.only(left: 40.0, top: 30, right: 10.0),
                    ),
                    new Flexible(child: new Scrollbar(child: new SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child:
                      new Text(Internados.animals[this.index].diagonostico, textAlign: TextAlign.left, style: TextStyle(fontSize: 20.0),),
                    )))
                  ],
                ),
                Row( children :[
                  botao_alta(index),
                  botao_alarme(Internados.animals[this.index].name)
                ]
                ),
              ]
          ),)
        ))
    );
  }
}

class botao_alta extends StatelessWidget{
  final int index;

  botao_alta(this.index);

  @override
  Widget build(BuildContext context){
    if(internados_description.nome_empregado == 'Enfermeiro Diogo') return new Container();
    return new Container(
      padding: new EdgeInsets.only(left: 10, top: 65, bottom: 10),
      child: new RaisedButton.icon(
        textColor: Colors.white,
        icon: Icon(Icons.remove),
        label: const Text('Dar Alta'),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(84),
        ),

        color: Colors.teal,
        onPressed: () {
          Internados.animals.removeAt(index);
          Navigator.of(context).pop();
        },
      ),
    );
  }
}

class botao_alarme extends StatelessWidget{
  final String name;

  botao_alarme(this.name);

  @override
  Widget build(BuildContext context){
    if(internados_description.nome_empregado == 'Enfermeiro Diogo') return new Container();
    return new Container(
      padding: new EdgeInsets.only(left: 120, top: 65, bottom: 10),
      child: new RaisedButton.icon(
        textColor: Colors.white,
        icon: Icon(Icons.add),
        label: const Text('Adicionar Tarefa'),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(84),
        ),
        color: Colors.teal,
        onPressed: () {
          /*Adicionar para lista de tarefas enfermeiro*/
          add_task.name = this.name;
          add_task.nome_empregado = 'Dr.Laura';
          Navigator.push(context, MaterialPageRoute( builder: (context) => add_task()));

        },
      ),
    );
  }
}
